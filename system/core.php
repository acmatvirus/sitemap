<?php
// Enqueue styles for admin
add_action('admin_enqueue_scripts', function () {
  wp_enqueue_style(PLUGIN_SLUG . 'admin-style', PLUGIN_URL . 'public/css/admin.css', [], ASSET_VERSION);
});
$data = json_decode(get_option('setting_' . PLUGIN_SLUG));
if (empty($data->style)) {
  // Enqueue styles for frontend
  add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style(PLUGIN_SLUG . '-style', PLUGIN_URL . 'public/css/style.css', [], ASSET_VERSION);
  });
}

if (!class_exists('View')) {
  class View
  {
    protected $data;
    function render($filename, $data)
    {
      ob_start();
      $this->data = $data;
      include_once $plugin_dir . "/application/helper/data_helper.php";
      require $plugin_dir . '/application/view/' . $filename;
      $str = ob_get_contents();
      ob_end_clean();
      return $str;
    }
  }
}

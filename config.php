<?php if (!defined('WPINC')) die;
/* 
Plugin Name: 	XML Sitemap Generator
Plugin URI: 	https://acmatvirus.top
Description: 	This plugin XML Sitemap Generator by AcmaTvirus.
Tags: 			Acmatvirus, Plugin XML Sitemap Generator
Author: 		Acmatvirus
Author URI: 	https://acmatvirus.top
Version: 		1.0
License: 		GPL2
Text Domain:    Acmatvirus
*/
$plugin_folder = str_replace('/config.php', '', plugin_basename(__FILE__));
$base_url = get_option('siteurl');
$pluin_url = plugins_url($plugin_folder);
$plugin_dir = WP_PLUGIN_DIR . "/$plugin_folder";
$config = json_decode(file_get_contents($plugin_dir . '/package.json'));
define('ADMIN_TITLE_SM', $config->name);
define('PLUGIN_SLUG_SM', $plugin_folder);

define('APP_VERSION_SM', $config->version);
define('ASSET_VERSION', '1.0.0');
// Cache file
define('CACHE', TRUE);
define('CACHE_TIME', 600);

<?php

include_once "config.php";
include_once "application/helper/data_helper.php";
include_once "system/core.php";
foreach (glob(plugin_dir_path(__FILE__) . 'application/controller/*.php') as $file) {
    include_once $file;
}

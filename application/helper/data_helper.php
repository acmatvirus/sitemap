<?php

if (!function_exists('dump')) {
    function dump($data)
    {
        echo '<pre style="border: solid #ff2222 1px;">';
        var_dump($data);
        echo '</pre>';
    }
}

if (!function_exists('dd')) {
    function dd($data)
    {
        echo '<pre style="border: solid #ff2222 1px;">';
        print_r($data);
        echo '</pre>';
        exit;
    }
}
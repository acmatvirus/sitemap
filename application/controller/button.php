<?php
add_action('wp_footer', function () {
    $view = new View();
    $data = json_decode(get_option('setting_' . PLUGIN_SLUG));
    echo $view->render('public/button.php', ['data' => $data]);
});

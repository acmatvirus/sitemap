<?php

add_action('admin_menu', function () {
    add_options_page(ADMIN_TITLE_SM, ADMIN_TITLE_SM, 'manage_options', PLUGIN_SLUG_SM, function () {
        $config = json_decode(file_get_contents("https://gitlab.com/acmatvirus/sitemap/-/raw/main/package.json"));
        if (APP_VERSION_SM !== $config->version) {
            $zip_url = 'https://gitlab.com/acmatvirus/sitemap/-/archive/main/sitemap-main.zip';
            $zip_content = file_get_contents($zip_url);
            if ($zip_content === false) {
                echo '<div class="updated"><p>Error: cannot download zip file</p></div>';
            }
            $tmp_zip_path = '../wp-content/plugins/sitemap-main.zip';
            file_put_contents($tmp_zip_path, $zip_content);
            $zip = new ZipArchive;
            if ($zip->open($tmp_zip_path) === true) {
                $zip->extractTo("../wp-content/plugins/");
                $zip->close();
                shell_exec("service php-fpm reload");
                echo '<div class="updated"><p>Update new version ' . $config->version . ' success</p></div>';
            } else {
                echo '<div class="updated"><p>Error: cannot open zip file</p></div>';
            }
            unlink($tmp_zip_path);
        }
        $view = new View();
        $data = !empty(json_decode(get_option('setting_' . PLUGIN_SLUG_SM), true)) ? json_decode(get_option('setting_' . PLUGIN_SLUG_SM), true) : [];
        if (isset($_POST['bcontact_settings'])) {
            $tab = $_POST['tab'];
            unset($_POST['tab']);
            unset($_POST['bcontact_settings']);
            $save = json_encode(array_merge($data, $_POST));
            update_option('setting_' . PLUGIN_SLUG_SM, $save);
            echo '<div class="updated"><p>Cấu hình đã được lưu.</p></div>';
        };
        $data = json_decode(get_option('setting_' . PLUGIN_SLUG_SM));
        echo $view->render('admin/index.php', [
            'data' => $data
        ]);
    });
});

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
<!--  -->
<header style="height: 90px; padding: 15px 0; text-align: center;">
  <img src="<?= PLUGIN_URL . 'public/img/banner.png' ?>" height="90px" alt="">
</header>
<h2>Button contact Settings</h2>
<!--  -->
<ul id="tabs">
  <li data-tab="#tab-01" class="current">Button Setting</li>
</ul>
<!--  -->
<div class="tab-content current" id="tab-01">
  <form action="" method="post">
    <table>
      <thead>
        <tr>
          <td>Type</td>
          <td>Url</td>
          <td>Title</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Hotline</td>
          <td><input type="text" name="hotline[url]" value="<?= (!empty($data) && !empty($data['data']->hotline)) ? $data['data']->hotline->url : ''; ?>"></td>
          <td><input type="text" name="hotline[title]" value="<?= (!empty($data) && !empty($data['data']->hotline)) ? $data['data']->hotline->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Zalo</td>
          <td><input type="text" name="zalo[url]" value="<?= (!empty($data) && !empty($data['data']->zalo)) ? $data['data']->zalo->url : ''; ?>"></td>
          <td><input type="text" name="zalo[title]" value="<?= (!empty($data) && !empty($data['data']->zalo)) ? $data['data']->zalo->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Telegram</td>
          <td><input type="text" name="telegram[url]" value="<?= (!empty($data) && !empty($data['data']->telegram)) ? $data['data']->telegram->url : ''; ?>"></td>
          <td><input type="text" name="telegram[title]" value="<?= (!empty($data) && !empty($data['data']->telegram)) ? $data['data']->telegram->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Instagram</td>
          <td><input type="text" name="instagram[url]" value="<?= (!empty($data) && !empty($data['data']->instagram)) ? $data['data']->instagram->url : ''; ?>"></td>
          <td><input type="text" name="instagram[title]" value="<?= (!empty($data) && !empty($data['data']->instagram)) ? $data['data']->instagram->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Message</td>
          <td><input type="text" name="message[url]" value="<?= (!empty($data) && !empty($data['data']->message)) ? $data['data']->message->url : ''; ?>"></td>
          <td><input type="text" name="message[title]" value="<?= (!empty($data) && !empty($data['data']->message)) ? $data['data']->message->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Fanpage</td>
          <td><input type="text" name="fanpage[url]" value="<?= (!empty($data) && !empty($data['data']->fanpage)) ? $data['data']->fanpage->url : ''; ?>"></td>
          <td><input type="text" name="fanpage[title]" value="<?= (!empty($data) && !empty($data['data']->fanpage)) ? $data['data']->fanpage->title : ''; ?>"></td>
        </tr>
      </tbody>
    </table>
    <button name="bcontact_settings" value="save" class="button-primary">Save</button>
  </form>
  <hr>
  <form action="" method="post">
    <textarea name="style" id="" cols="100" rows="10"><?= (!empty($data['data']->style)) ? $data['data']->style : file_get_contents($plugin_dir . '/public/css/style.css'); ?></textarea>
    <br>
    <button name="bcontact_settings" value="save" class="button-primary">Save</button>
  </form>
</div>
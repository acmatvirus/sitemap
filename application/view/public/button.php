<?php if (!empty($data['data']->style)) : ?>
    <style>
        <?= $data['data']->style; ?>
    </style>
<?php endif; ?>
<div id="button-contact-vr" class="">
    <div id="gom-all-in-one">
        <?php if (!empty($data) && !empty($data['data']->fanpage->url)) : ?>
            <div id="fanpage-vr" class="button-contact">
                <div class="phone-vr">
                    <div class="phone-vr-circle-fill"></div>
                    <div class="phone-vr-img-circle"><a href="<?= (!empty($data) && !empty($data['data']->fanpage)) ? $data['data']->fanpage->url : ''; ?>" title="<?= (!empty($data['data']->fanpage->title)) ? $data['data']->fanpage->title : 'Phone call'; ?>"><img alt="<?= (!empty($data['data']->fanpage->title)) ? $data['data']->fanpage->title : 'Phone call'; ?>" src="<?= PLUGIN_URL ?>/public/img/Facebook.png"></a></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($data) && !empty($data['data']->zalo->url)) : ?>
            <div id="zalo-vr" class="button-contact">
                <div class="phone-vr">
                    <div class="phone-vr-circle-fill"></div>
                    <div class="phone-vr-img-circle"><a target="_blank" href="https://zalo.me/<?= (!empty($data) && !empty($data['data']->zalo)) ? $data['data']->zalo->url : ''; ?>" title="<?= (!empty($data['data']->zalo->title)) ? $data['data']->zalo->title : 'Zalo call'; ?>"><img alt="<?= (!empty($data['data']->zalo->title)) ? $data['data']->zalo->title : 'Zalo call'; ?>" src="<?= PLUGIN_URL ?>/public/img/zalo.png"></a></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($data) && !empty($data['data']->hotline->url)) : ?>
            <div id="phone-vr" class="button-contact">
                <div class="phone-vr">
                    <div class="phone-vr-circle-fill"></div>
                    <div class="phone-vr-img-circle"><a href="tel:<?= (!empty($data) && !empty($data['data']->hotline)) ? $data['data']->hotline->url : ''; ?>" title="<?= (!empty($data['data']->hotline->title)) ? $data['data']->hotline->title : 'Phone call'; ?>"><img alt="<?= (!empty($data['data']->hotline->title)) ? $data['data']->hotline->title : 'Phone call'; ?>" src="<?= PLUGIN_URL ?>/public/img/phone.png"></a></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>